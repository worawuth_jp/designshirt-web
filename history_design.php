<?php
session_start();
include "function.php";
$id = $_GET['mb_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <title>ร้านค้าออนไลน์</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
    <link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="css_style_index.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_menu.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_board.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_page.css"/>

    <style type="text/css">
    </style>


</head>
<body id="Page5">
<div id="container">
    <div id="bander_front">
        <?PHP include "bander_front.php"; ?>
        <div id="menu_top">
            <p>
                <?PHP include "menu_top1.php"; ?>
            </p>
        </div>
    </div>

    <div class="menu_left"><!-- เมนูด้านซ้าย -->
        <?PHP include "menu_left_front.php"; ?>
    </div><!-- จบเมนูด้านซ้าย -->

    <div class="data_center"><!-- ส่วนกลางของเว็บ -->
        <div class="data_center_back">
            <table width="100%" height="500" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top">
                        <div class="title">
                            <h2><img src="images/clipboard_32.png" width="48" height="48"/> ประวัติการออกแบบ</h2>
                        </div>
                        <br>
                        <a href="design_buy_list.php?mb_id=<?php echo $_SESSION['sess_mb_id']?>"><button style="margin-left: 10px">รายการสั่งซื้อ</button></a>
                        <br><br>
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <thead>
                                <th>#</th>
                                <th>รูปที่ออกแบบ</th>
                                <th>ราคา</th>
                                <th></th>
                                </thead>
                            </tr>
                            <?php
                            $sql = "SELECT * FROM design WHERE mb_id='$id'";
                            $result = $con->query($sql);
                            $count = 0;
                            while ($row = $result->fetch_assoc()) {
                                $count = $count + 1;
                                ?>
                                <tr>
                                    <td style="text-align: center"><?php echo $count; ?></td>
                                    <td style="text-align: center"><img height="180px" src="<?php echo $row['pic'] ?>">
                                    </td>

                                    <td style="text-align: center"><?php echo $row['price'] ?></td>
                                    <td style="text-align: center">
                                        <a href="design_buy.php?id=<?php echo $row['id'] ?>">
                                            <button>สั่งซื้อ</button>
                                        </a></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>

                </tr>
            </table>
            <p>&nbsp;</p>
        </div>

        <!-- เมนูด้านซ้าย -->
        <p style="clear:both;"></p>
        <!-- ปิด เมนูด้านซ้าย -->

    </div>
    <div id="footer_front">
        <div class="data_footer">
            <p>
                <?PHP include "footer.php"; ?>
            </p>

        </div>

    </div>
    <div style="clear:both;"></div>
    <!-- End menu -->
</div>
<!-- end Container -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php
if (isset($_GET['status'])) {
if ($_GET['status'] == 1){
    ?>
    <script type="text/javascript">
        swal("Success", "สั่งซื้อสำเร็จ", "success");
    </script>
<?php
}

if ($_GET['status'] == 0){
?>
    <script type="text/javascript">
        swal("ผิดพลาด", "สั่งซื้อไม่สำเร็จ", "error");
    </script>
    <?php
}
}
?>
</body>
</html>
