var count_text = 0;
var count_pic = 0;
var numText = 0;
var numPic = 0;
var isResize = false;
var canMove = true;
let base_url = './images/temp/o/';
let front_white = base_url + '1.JPG';
let back_white = base_url + '2.JPG';
let left_white = base_url + '3.JPG';
let right_white = base_url + '4.JPG';
var preview_shirt = new Object();
preview_shirt = {front: front_white, back: back_white, left: left_white, right: right_white};

$('#reset').click(function () {
    $('#areabox').html('');
    count_text = 0;
    count_pic = 0;
});

function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete(parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    color = '#' + parts.join('');

    return color;
}

function priceChange(op){
    var price = document.getElementById('showprice').innerText;
    if(numText > 0 && op=='-'){
        if(numText < 3){
            document.getElementById('showprice').innerText = numeral(parseFloat(price)-55).format('0,0.00');
        }else{
            document.getElementById('showprice').innerText = numeral(parseFloat(price)-65).format('0,0.00');
        }
        numText--;
    }

    if(numText >= 0 && op=='+'){
        numText++;
        if(numText < 3){
            document.getElementById('showprice').innerText = numeral(parseFloat(price)+55).format('0,0.00');
        }else{
            document.getElementById('showprice').innerText = numeral(parseFloat(price)+65).format('0,0.00');
        }
    }
}

function delBox(id) {
    priceChange('-');

    if(numPic > 0){
        var el = document.getElementById(id);
        var rel = el.getBoundingClientRect();
        var price = numeral(document.getElementById('showprice').innerText).value();

        if(rel.width > 0.35*document.getElementById('areabox').getBoundingClientRect().width){
            price = price - 80;
        }else{
            price = price - 60;
        }

        document.getElementById('showprice').innerText = numeral(price).format('0,0.00');
    }

    document.getElementById(id).remove();
    $('#TEXT-2').addClass('hidden-filed');
}

function change_preview(id) {
    $('#preview_img').css('background-image', 'url(' + preview_shirt[id] + ')');
}

function change_color(p1, p2, p3, p4) {
    $('#preview_img').css('background-image', 'url(' + p1 + ')');
    preview_shirt = {front: p1, back: p2, left: p3, right: p4};
    /*$('#F').attr('src', p1);
    $('#B').attr('src', p2);
    $('#L').attr('src', p3);
    $('#R').attr('src', p4);*/
}

$('#btn-tool').click(function () {
    if ($('.btn-tool-open').length) {
        $('#btn-tool').removeClass('btn-tool-open').addClass('btn-tool');
        $('#tool').removeClass('open-tool').addClass('hidden-tool');
        $('#btn-tool').addClass('fa-angle-double-left').removeClass('fa-angle-double-right');
    } else {
        $('#btn-tool').removeClass('btn-tool').addClass('btn-tool-open');
        $('#tool').removeClass('hidden-tool').addClass('open-tool');
        $('#btn-tool').addClass('fa-angle-double-right').removeClass('fa-angle-double-left');
    }
});

$('.text-tool').click(function () {
    if ($('#show_text').hasClass('hidden-filed')) {
        $('#show_text').removeClass('hidden-filed');
    }
    if ($('#TEXT').hasClass('hidden-filed')) {
        $('#TEXT').removeClass('hidden-filed');
        $(this).css('background-color', '#D0D0D0');

    } else {
        $('#TEXT').addClass('hidden-filed');
        $(this).css('background-color', '');
    }


    $('.pic-tool').css('background-color', '');
    $('#PIC').addClass('hidden-filed');

    if ($('#areabox').hasClass('border-text') == false)
        $('#areabox').addClass('border-text');
});

var loadFile = function (event) {
    /*alert(URL.createObjectURL(event.target.files[0]));*/

    if(event.target.files.length > 0){
        let src = URL.createObjectURL(event.target.files[0]);
        let id = 'pic-'+(++count_pic);
        let nodeid = 'node'+id;
        let res_html = "<div id=\"resizing" + id + "\" class=\"border-fill cursor-resize text-center line-tag p-1\" style=\"position:absolute;right: 0;bottom: 0; width:auto;height:auto;font-size: 12px;text-align: center;color: red\"><i class=\"fa fa-expand\"></i></div>";
        let close_html = "<div id=\'close"+id+"\' onclick=\"delBox('" + nodeid + "')\" class=\"border-fill cursor-hand line-tag\" style=\"position:absolute;left: 0;top: 0; width:16px;height:auto;font-size: 12px;text-align: center;color: red\">X</div>";
        var html = "<div class='p-0 text-drag box-tag' id=\""+nodeid+"\"><img src='' id=\""+id+"\">"+res_html+close_html+"</div>";

        $('#areabox').append(html);
        $('#areabox').removeClass('hidden-filed');
        var img = document.getElementById(id);
        var picbox = document.getElementById(nodeid);
        img.classList.add("cursor-all-scroll","p-0");
        picbox.classList.add("border-sq-text");
        img.style.width = '80px';
        img.src = src;
        dragElement(picbox);
        resizer_img(picbox,document.getElementById('resizing'+id),document.getElementById(id));

        picbox.addEventListener('click',function (){
            $('#TEXT-2').addClass('hidden-filed');
        });
    }

    /*if (event.target.files.length > 0) {
        var src = URL.createObjectURL(event.target.files[0]);
        let areaText = $('#areabox');
        let id = 'pic-'+(++count_pic);
        let html = "<img id=\""+id+"\" >";
        areaText.append(html);
        var preview = document.getElementById(id);
        preview.src = src;
        $('#areabox').removeClass('hidden-filed');
    }*/

};

$('.pic-tool').click(function () {
    if ($('#PIC').hasClass('hidden-filed')) {
        $('#PIC').removeClass('hidden-filed');
        $('#TEXT-2').addClass('hidden-filed');
        $(this).css('background-color', '#D0D0D0');
    } else {
        $(this).css('background-color', '');
        $('#PIC').addClass('hidden-filed');
    }
    $('.text-tool').css('background-color', '');
    $('#TEXT').addClass('hidden-filed');
});


function createText(txt) {
    let id = "text-" + ++count_text;
    let res_html = "<div id=\"resizing" + id + "\" class=\"border-fill cursor-resize text-center line-tag p-1\" style=\"position:absolute;right: 0;bottom: 0; width:auto;height:auto;font-size: 12px;text-align: center;color: red\"><i class=\"fa fa-expand\"></i></div>";
    let close_html = "<div id=\'close"+id+"\' onclick=\"delBox('" + id + "')\" class=\"border-fill cursor-hand line-tag\" style=\"position:absolute;left: 0;top: 0; width:16px;height:auto;font-size: 12px;text-align: center;color: red\">X</div>";
    let rot_html = "";
    let element = document.getElementById('fsize');
    element.onchange = function () {
        sizeChange(element);
    };

    let font = $('#font-prop').val();
    let html = "<div id=\"" + id + "\" class=\"border-sq-text text-drag text-center box-tag p-0\" ><p id=\"size" + id + "\" onclick='getID(this.id)' class=\"cursor-all-scroll p-0 "+font+"\">" + txt + "</p>" + close_html + res_html + "</div>";
    $('#areabox').append(html);
    $('#tellID').val("size"+id);
    $('#picsize').val(document.getElementById('size'+id).style.fontSize);
    console.log($('#picsize').val());
    dragElement(document.getElementById(id));
    resizer(document.getElementById(id), document.getElementById('resizing' + id), document.getElementById("size" + id));
    propText(document.getElementById(id), txt, document.getElementById("size" + id));
    document.getElementById(id).style.height = "auto";
    document.getElementById('size'+id).style.height = "auto";
    //dragItem(document.getElementById(id));

}
function getID(id){
    let nodeid = document.getElementById(id).parentNode.id;
    let el = document.getElementById(id);
    console.log($('#tellID').css('color'));

    colorWell.value = hexc($('#'+$('#tellID').val()).css('color'));
    propText(document.getElementById(nodeid),document.getElementById(id).innerText,el);
}

function changeFontText(el,font){
    el.removeClass();
    el.addClass('cursor-all-scroll').addClass(font);
}

$('#btn-text-on').click(function () {
    var txt = $('#txt').val();

    createText(txt);
    $('#txt').val("");

    $('#areabox').removeClass('hidden-filed');
    btnClick($('#TEXT'));
    colorWell.value = defaultColor;

    //add price
    priceChange('+');
});

function removeAllLine(el){
    var x = el.querySelectorAll(".line-tag");
    for(var i=0;i<x.length;i++){
        console.log(x[i].id);
        x[i].classList.add("hidden-filed");
    }
    x = el.querySelectorAll(".box-tag");
    for(var i=0;i<x.length;i++){
        console.log(x[i].id);
        x[i].classList.remove("border-sq-text");
    }
    document.getElementById('areabox').classList.remove('border-text');
}

$('#btn-suc').click(function () {
    if ($('#areabox').hasClass('border-text')) {
        $('#TEXT-2').addClass('hidden-filed');
        $('#areabox').removeClass('border-text');
        $('.text-tool').css('background-color', '');
    }
});

$('#up_logo').click(function () {
    $('#img').click();
});

function btnClick(el) {
    if(el.hasClass('hidden-filed')){
        el.css('background-color','#D0D0D0');
    }else{
        el.css('background-color','');
    }

}

/*dragElement(document.getElementById("show_text"));

dragElement(document.getElementById("pre-pic"));*/
function resizer_img(el, re, img) {
    let currentResizer;
    var rate;
    var price = numeral(document.getElementById('showprice').innerText).value();
    var ig = document.getElementById(el.id).getBoundingClientRect();
    var  areag = document.getElementById('areabox').getBoundingClientRect();
    console.log(ig.width);
    console.log('el: '+ig.width+' area: '+0.40*areag.width);
    if(ig.width > (0.35*areag.width)){
        price += 80;
        rate = 80;
    }else{
        price += 60;
        rate = 60;
    }
    numPic++;
    document.getElementById('showprice').innerText = numeral(price).format('0,0.00');

    re.addEventListener('mousedown', mousedown);

    function mousedown(e) {
        currentResizer = e.target;
        isResize = true;
        let prevX = e.clientX;
        let prevY = e.clientY;

        window.addEventListener('mousemove', mousemove);
        window.addEventListener('mouseup', mouseup);

        function mousemove(e) {
            const rect = el.getBoundingClientRect();
            const node = document.getElementById('areabox').getBoundingClientRect();
            const te = img.getBoundingClientRect();

            if (rect.width <= node.width && rect.height <= node.height) {
                el.style.width = rect.width - (prevX - e.clientX) + 'px';
                el.style.height = rect.height - (prevY - e.clientY) + 'px';
                priceChange('p+')
            } else {
                el.style.width = rect.width + (prevX - e.clientX) + 'px';
                el.style.height = rect.height + (prevY - e.clientY) + 'px';
                priceChange('p-')
            }
            $('#picsize').val(el.style.height);
            console.log($('#picsize').val());

            /*el.style.height = 'auto';
            el.style.width = 'auto';*/

            /*let front_size = window.getComputedStyle(img, null).getPropertyValue('font-size');
            front_size = parseInt(front_size) - (prevY - e.clientY);
            el.style.width = te.width+ 'px';
            el.style.height = te.height + 'px';
            sizefont.style.fontSize = parseInt(front_size) + 'px';
            $('#fsize').val(front_size);
            console.log("prevY : " + prevY + " clientY : " + e.clientY);*/

            img.style.width = el.style.width;
            el.style.height = 'auto';

            prevX = e.clientX;
            prevY = e.clientY;

        }

        function mouseup() {
            price = numeral(document.getElementById('showprice').innerText).value();
            ig = img.getBoundingClientRect();
            areag = document.getElementById('areabox').getBoundingClientRect();

            if(ig.width > (0.35*areag.width)){
                price = price-rate;
                price = price+80;
                rate = 80;
            }else{
                price = price-rate;
                price = price+60;
                rate = 60;
            }

            document.getElementById('showprice').innerText = numeral(price).format('0,0.00');
            console.log('ig: '+ig.width+' area: '+0.35*areag.width);
            console.log(price);
            window.removeEventListener('mousemove', mousemove);
            window.removeEventListener('mouseup', mouseup);
            isResize = false;
        }
    }


}

function resizer(el, re, sizefont) {
    let currentResizer;

    re.addEventListener('mousedown', mousedown);

    function mousedown(e) {
        currentResizer = e.target;
        isResize = true;
        let prevX = e.clientX;
        let prevY = e.clientY;

        window.addEventListener('mousemove', mousemove);
        window.addEventListener('mouseup', mouseup);

        function mousemove(e) {
            const rect = el.getBoundingClientRect();
            const node = document.getElementById('areabox').getBoundingClientRect();
            /*const te = sizefont.getBoundingClientRect();*/

            if (rect.width <= node.width && rect.height <= node.height) {
                el.style.width = rect.width - (prevX - e.clientX) + 'px';
                el.style.height = rect.height - (prevY - e.clientY) + 'px';
            } else {
                el.style.width = rect.width + (prevX - e.clientX) + 'px';
                el.style.height = rect.height + (prevY - e.clientY) + 'px';
            }
            /*el.style.height = 'auto';
            el.style.width = 'auto';*/

            let front_size = window.getComputedStyle(sizefont, null).getPropertyValue('font-size');
            front_size = parseInt(front_size) - (prevY - e.clientY);
            /*el.style.width = te.width+ 'px';
            el.style.height = te.height + 'px';*/
            sizefont.style.fontSize = parseInt(front_size) + 'px';
            $('#fsize').val(front_size);
            console.log("prevY : " + prevY + " clientY : " + e.clientY);

            sizefont.style.height = 'auto';
            sizefont.style.width = 'auto';
            el.style.height = 'auto';
            el.style.width = 'auto';
            el.style.paddingTop = '0px';
            el.style.paddingBottom = '0px';

            prevX = e.clientX;
            prevY = e.clientY;

        }

        function mouseup() {
            window.removeEventListener('mousemove', mousemove);
            window.removeEventListener('mouseup', mouseup);
            isResize = false;
        }
    }


}

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    if(canMove == true){
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {

        document.getElementById('tellID').value = elmnt.id;

        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        if (!isResize) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;

            //el
            let childEL = document.getElementById($('#tellID').val()).childNodes;
            console.log(childEL[0].id);

            // set the element's new position:
            let start_h = document.getElementById('areabox').offsetTop-childEL[0].offsetTop;
            let h = start_h + document.getElementById('areabox').offsetHeight - childEL[0].offsetHeight ;
            let sw = document.getElementById('areabox').offsetLeft;
            let w = sw + document.getElementById('areabox').offsetWidth - childEL[0].offsetWidth ;
            if ((elmnt.offsetTop - pos2) >= start_h && (elmnt.offsetTop - pos2) <= h) {
                elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            }
            if ((elmnt.offsetLeft - pos1) >= sw && (elmnt.offsetLeft - pos1) <= w) {
                elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
            }
        }
    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

function sizeChange(el) {
    let size = window.getComputedStyle(el, null).getPropertyValue('font-size');
    el.style.fontSize = size + 'px';

}

var colorWell;
var defaultColor = "#000000";
window.addEventListener("load", TextColorChange, false);

function TextColorChange() {
    colorWell = document.querySelector("#color_picker");
    let ids1 = document.getElementById($('#tellID').val());
    colorWell.value = defaultColor;
    colorWell.addEventListener("input", updateFirst, false);
    colorWell.addEventListener("change", updateAll, false);
    colorWell.select();
}

function updateFirst(event) {
    var show_text = document.getElementById($('#tellID').val());

    if (show_text) {
        show_text.style.color = event.target.value;
    }
}

function updateAll(event) {
    let show_text = document.getElementById($('#tellID').val());
        show_text.style.color = event.target.value;
}

$('#btn-save').click(function () {
    $('#btn-suc').click();
    removeAllLine(document.getElementById('areabox'));
    canMove = false;

    /*var fd = new FormData();
    var files = $('#img')[0].files[0];
    fd.append('file',files);*/

    /*$.ajax({
        url: 'upload.php',
        type: 'post',
        data : fd,
        contentType: false,
        processData: false,
        success: function (data){
            let img = document.getElementById('pic-'+(count_pic));
            let nodeimg = document.getElementById('pic-'+(count_pic)).parentNode;
            nodeimg.classList.remove("border-sq-text")
            nodeimg.style.backgroundImage = "url("+data+")";
            img.src = data;
        },
    });*/

    html2canvas(document.getElementById('preview_img'), { logging: true, letterRendering: 1, allowTaint: false, useCORS: true}).then(canvas => {
        canvas.setAttribute('id','canvas');
        var dataUrl = canvas.toDataURL();
        var fd = new FormData();
        var price = "ราคา "+document.getElementById('showprice').innerText+" บาท";
        fd.append('file',dataUrl);
        fd.append('price',document.getElementById('showprice').innerText);


        $.ajax({
            url: "upload.php",
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            success: function (){
                swal("บันทึกข้อมูลสำเร็จ",price, "success").then(function (){
                    location.reload();
                });

            },
        });
    });

});

/*function doCapture() {
    html2canvas(document.getElementById('preview_img')).then(function (canvas) {
        console.log(canvas.toDataURL("images/png"));

        var ajax = new XMLHttpRequest();
        ajax.open("POST", "save-capture.php", true);
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("image=" + canvas.toDataURL("image/png"));

    });
}*/

function propText(el, txt,id_el) {
    let elText = document.getElementById('txt-prop');
    let T2 = document.getElementById('TEXT-2');
    let font_el = document.getElementById('font-prop');
    let fsize = document.getElementById('fsize');

    T2.classList.remove('hidden-filed');
    elText.value = txt;

    elText.addEventListener('keypress',function () {
        document.getElementById('size'+$('#tellID').val()).innerHTML = elText.value;
    });
    font_el.addEventListener('change',function () {
        let textEL = document.getElementById('size'+$('#tellID').val());
        let nodeEL = document.getElementById($('#tellID').val());
        textEL.removeAttribute("class");
        textEL.classList.add("cursor-all-scroll",font_el.value);
        nodeEL.style.height = 'auto';
        nodeEL.style.width = 'auto';
    });
    let getFontSize = parseInt(window.getComputedStyle(id_el, null).getPropertyValue('font-size'));
    fsize.value = getFontSize;
    fsize.addEventListener("change",function () {
        document.getElementById('size'+$('#tellID').val()).style.fontSize = fsize.value + 'px';
    });


}

$('#font-prop').change(function () {
    font_select($('#font-prop').val(),$('#font-prop'));
});
function font_select(value,el){
    document.getElementById("font-prop").removeAttribute("class");
    el.addClass("col-md-10").addClass("mb-2").addClass("pl-0");
    el.addClass(value);
    changeFontText($('#size'),value);
}

$('#stype').change(function (){
    let svalue = $('#stype').val();
    var preview = 'url(\'./images/temp/o/1.JPG\')';
    if(svalue == 2){
        preview = 'url(\'./images/temp/v/1.JPG\')';
    }
    else if(svalue == 3){
        preview = 'url(\'./images/temp/po/1.JPG\')';
    }
    document.getElementById('preview_img').style.backgroundImage = preview;
    var d;
    var fd = new FormData();
    fd.append('value',svalue);
   /* $.ajax({
        url: 'stype.php',
        type: 'POST',
        data: fd,
        contentType: false,
        processData: false,
        success: function (data){
            d = JSON.parse(data);
            if(d[0].id != -1)
                changeShirt(d);
        },
    });*/
});

function changeShirt(data){
    //console.log(data[0].frontside);
    document.getElementById('F').src = './'+data[0].frontside;
    document.getElementById('B').src = './'+data[0].backside;
    document.getElementById('L').src = './'+data[0].leftside;
    document.getElementById('R').src = './'+data[0].rightside;

    preview_shirt = new Object();
    preview_shirt = {front:data[0].frontside,back:data[0].backside,left:data[0].leftside,right:data[0].rightside};

    var html='';
    var count = 0;
    data.forEach(myfunc);

    function myfunc(item,index){
        if(count==0){
            html += '<div class=\"row pl-1 pt-1\">';
        }
        console.log(item.frontside);
        html += ("<a href=\"#\" onclick=\"change_color('"+item.frontside+"','"+item.backside+"','"+item.leftside+"','"+item.rightside+"')\"><div class=\"border mt-1 ml-1\" style=\"height: 20px;width: 20px;background-color: "+item.color+" \"></div></a>");

        count++;
        if(count == 3){
            html += '</div>';
            count = 0;
        }
    }
    $('#cselect').html(html);
}
