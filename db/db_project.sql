-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2021 at 07:44 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `bn_id` int(11) NOT NULL,
  `bn_bank` varchar(150) NOT NULL,
  `bn_name` varchar(150) NOT NULL,
  `bn_number` varchar(150) NOT NULL,
  `bn_branch` varchar(150) NOT NULL,
  `bn_photo` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bn_id`, `bn_bank`, `bn_name`, `bn_number`, `bn_branch`, `bn_photo`) VALUES
(9, 'ไทยพาณิชย์', 'แก้วใจ นมขุนทด', '6662357996', 'ถนนมิตรภาพ', '14115379370.png');

-- --------------------------------------------------------

--
-- Table structure for table `board_answer`
--

CREATE TABLE `board_answer` (
  `ans_id` int(11) NOT NULL,
  `ans_IDtopic` int(11) NOT NULL,
  `ans_name` varchar(50) NOT NULL,
  `ans_email` varchar(100) NOT NULL,
  `ans_detail` text NOT NULL,
  `ans_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `board_answer`
--

INSERT INTO `board_answer` (`ans_id`, `ans_IDtopic`, `ans_name`, `ans_email`, `ans_detail`, `ans_date`) VALUES
(9, 3, 'สุริยา จันทะนาลา545', 'aom.j@hotmail.com', 'ขายแผ่น CD และ DVD ', '2012-08-26 22:01:14'),
(10, 3, 'สุริยา จันทะนาลา545', 'aom.j@hotmail.com', 'ขายแผ่น CD และ DVD ', '2012-08-26 22:01:48'),
(13, 3, 'ผู้ดูแลระบบ', 'aom.j@hotmail.com', 'หกดหกดหกดหกด', '2012-09-09 02:38:25');

-- --------------------------------------------------------

--
-- Table structure for table `board_question`
--

CREATE TABLE `board_question` (
  `topic_id` int(11) NOT NULL,
  `topic_name` varchar(50) NOT NULL,
  `topic_title` varchar(150) NOT NULL,
  `topic_email` varchar(100) NOT NULL,
  `topic_num` int(11) NOT NULL,
  `topic_photo` varchar(20) NOT NULL,
  `topic_detail` text NOT NULL,
  `topic_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `board_question`
--

INSERT INTO `board_question` (`topic_id`, `topic_name`, `topic_title`, `topic_email`, `topic_num`, `topic_photo`, `topic_detail`, `topic_date`) VALUES
(3, 'สุริยา จันทะนาลา', 'นี้เว็บเกี่ยวกับอะไรหรอ', 'aom.j@hotmail.com', 24, '003.png', 'นี้เว็บเกี่ยวกับอะไรหรอนี้เว็บเกี่ยวกับอะไรหรอ', '2012-08-26 20:28:16');

-- --------------------------------------------------------

--
-- Table structure for table `design`
--

CREATE TABLE `design` (
  `id` bigint(20) NOT NULL,
  `mb_id` bigint(20) NOT NULL,
  `pic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `date` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `isDel` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `design`
--

INSERT INTO `design` (`id`, `mb_id`, `pic`, `price`, `date`, `status`, `isDel`) VALUES
(1, 4, 'images/uploads/จันทร์ มีสี31012021160114.PNG', '205.00', '2020-10-06', 0, 0),
(2, 0, 'images/uploads/06102020171055.PNG', '205.00', '2020-10-06', 0, 0),
(6, 4, 'images/uploads/ทดสอบผู้ใช้ระบบ205022021230251.PNG', '260.00', '2021-02-05', 0, 0),
(7, 0, 'images/uploads/09022021010246.PNG', '205.00', '2021-02-09', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `design_order`
--

CREATE TABLE `design_order` (
  `ord_id` int(11) NOT NULL,
  `ord_idmb` int(11) NOT NULL,
  `ord_design_id` int(11) NOT NULL,
  `ord_total` decimal(12,2) NOT NULL,
  `ord_date` timestamp NULL DEFAULT current_timestamp(),
  `ord_status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `design_order`
--

INSERT INTO `design_order` (`ord_id`, `ord_idmb`, `ord_design_id`, `ord_total`, `ord_date`, `ord_status`) VALUES
(2, 4, 6, '260.00', '2021-02-05 17:23:17', 'Y'),
(3, 4, 1, '205.00', '2021-02-05 17:52:22', 'N'),
(5, 4, 1, '205.00', '2021-02-05 17:23:17', '2'),
(6, 4, 6, '260.00', '2021-02-05 17:23:17', '2'),
(8, 0, 7, '205.00', '2021-02-08 18:42:46', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `design_order_detail`
--

CREATE TABLE `design_order_detail` (
  `od_id_order` int(11) NOT NULL,
  `od_id_prd` int(11) NOT NULL,
  `od_num` int(11) NOT NULL,
  `od_approve` int(11) NOT NULL,
  `od_price` decimal(12,2) NOT NULL,
  `od_status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ems`
--

CREATE TABLE `ems` (
  `ems_id` int(5) NOT NULL,
  `ems_name` varchar(200) NOT NULL,
  `ems_price` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ems`
--

INSERT INTO `ems` (`ems_id`, `ems_name`, `ems_price`) VALUES
(2, 'ไปรษณีย์แบบลงทะเบียน (registered) ', 50),
(3, 'ไปรษณีย์แบบด่วนพิเศษ (EMS)', 100);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `mb_id` bigint(20) NOT NULL,
  `mb_user` varchar(50) NOT NULL,
  `mb_pass` varchar(20) NOT NULL,
  `mb_name` varchar(150) NOT NULL,
  `mb_sex` varchar(5) NOT NULL,
  `mb_birthday` date NOT NULL,
  `mb_address` text NOT NULL,
  `mb_tel` varchar(20) NOT NULL,
  `mb_email` varchar(150) NOT NULL,
  `mb_photo` varchar(20) NOT NULL,
  `mb_status` varchar(5) NOT NULL,
  `mb_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`mb_id`, `mb_user`, `mb_pass`, `mb_name`, `mb_sex`, `mb_birthday`, `mb_address`, `mb_tel`, `mb_email`, `mb_photo`, `mb_status`, `mb_date`) VALUES
(4, 'user', '1234', 'ทดสอบผู้ใช้ระบบ2', 'f', '2013-05-03', 'ร้อยเอ็ด  ทดสอบผู้ใช้ระบบ                                                                                                                   ', '0801813681', 'aom.j@hotmail.com', '15979710660.png', 'M', '2012-06-09'),
(6, 'admin', '1234', 'admin', 'f', '2001-02-06', '5555 / 88 หมู่ 5 ตำบล ปอภาร อำเภอเอมือง ร้อยเอ็ด                                                                                                                                                             ', '0801813681', 'aom.j@hotmail.com', '15314422660.png', '1', '2012-07-13'),
(8, 'root', '1234', 'มณีรัตน์ เรือทอง', '0000-', '0000-00-00', ' 555/ 88 แก้วน้ำ1แก้วน้ำ1แก้ว... อ่านต่อ\r\nราคา 500.00 บาท 450021', '0801813681', 'aom.j@hotmail.com', '1345959247.jpg', '1', '2012-08-26'),
(9, 'emp1', '1234', 'ขนมทองหยิบ', '0000-', '0000-00-00', '5555/7777 ร้อยเอ็ด', '0801813681', 'aom.j@hotmail.com', '1345960546.jpg', '2', '2012-08-26'),
(10, 'emp2', '1234', 'หมวกรายสีดำ', '0000-', '0000-00-00', '54646546 /44 ร้อยเอ็ด 55555                          ', '0801813681', 'aom.j@hotmail.com', '1345961923.jpg', '2', '2012-08-26'),
(11, 'user55', '1234', 'ทดสอบผู้ใช้ระบบ', 'm', '2013-05-01', 'ร้อยเอ็ด 555                                                ', '0801813681', 'aom.j@hotmail.com', '13652105040.jpg', 'M', '2012-08-29'),
(12, 'user222', '1234', 'ทดสอบ', 'm', '1987-07-02', 'ดหกด กห ดหก ดกหดกหด หกด กหด หกด กหด หกดกห ด                                                                                                                                                ', '0801813681', 'tes@hotmail.com', '1366956243.jpg', 'M', '2013-04-26');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `prd_id` int(10) NOT NULL,
  `prd_name` varchar(100) NOT NULL,
  `prd_type` varchar(5) NOT NULL,
  `prd_detail` text NOT NULL,
  `prd_price` float NOT NULL,
  `prd_stock` int(10) NOT NULL,
  `prd_sell` int(10) NOT NULL,
  `prd_photo` varchar(25) NOT NULL,
  `prd_status` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`prd_id`, `prd_name`, `prd_type`, `prd_detail`, `prd_price`, `prd_stock`, `prd_sell`, `prd_photo`, `prd_status`) VALUES
(4, 'ตัวอย่างสินค้า4', '3', 'ตัวอย่างสินค้า4', 150, 100, 2, '15979708570.jpg', '1'),
(5, 'ตัวอย่างสินค้า3', '7', 'ตัวอย่างสินค้า3', 100, 104, 1, '15979708500.jpg', '1'),
(3, 'ตัวอย่างสินค้า5', '4', 'ตัวอย่างสินค้า5', 150, 110, 0, '15979708630.jpg', '1'),
(6, 'ตัวอย่างสินค้า2', '3', 'ตัวอย่างสินค้า2', 55, 28, 2, '15979708450.jpg', '1'),
(7, 'ตัวอย่างสินค้า1', '3', 'ตัวอย่างสินค้า1', 80, 100, 0, '15979708400.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `type_id` int(10) NOT NULL,
  `type_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`type_id`, `type_name`) VALUES
(3, 'ประเภทสินค้า 1'),
(4, 'ประเภทสินค้า 2'),
(7, 'ประเภทสินค้า 3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `ord_id` int(11) NOT NULL,
  `ord_idems` int(11) NOT NULL,
  `ord_idmb` int(11) NOT NULL,
  `ord_name` varchar(150) NOT NULL,
  `ord_tel` varchar(15) NOT NULL,
  `ord_total` float NOT NULL,
  `ord_date` datetime NOT NULL,
  `ord_status` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`ord_id`, `ord_idems`, `ord_idmb`, `ord_name`, `ord_tel`, `ord_total`, `ord_date`, `ord_status`) VALUES
(1, 0, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 150, '2014-09-27 18:32:20', '3'),
(2, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 150, '2017-11-24 16:42:53', '3'),
(3, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:18', 'Y'),
(4, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:20', 'Y'),
(5, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:24', 'Y'),
(6, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:24', 'Y'),
(7, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:25', 'Y'),
(8, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:25', 'Y'),
(9, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:25', 'Y'),
(10, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:25', 'Y'),
(11, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:26', 'Y'),
(12, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:26', 'Y'),
(13, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:29', 'Y'),
(14, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:29', 'Y'),
(15, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:30', 'Y'),
(16, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:30', 'Y'),
(17, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:31', 'Y'),
(18, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:14:31', '3'),
(19, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 255, '2018-06-09 17:14:55', 'Y'),
(21, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 255, '2018-06-09 17:16:59', '3'),
(22, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 255, '2018-06-09 17:19:04', 'Y'),
(23, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:19:32', 'Y'),
(24, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:20:15', 'Y'),
(25, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:16', 'Y'),
(26, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:21', 'Y'),
(27, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:23', 'Y'),
(28, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:25', 'Y'),
(29, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:40', 'Y'),
(30, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:21:58', 'Y'),
(31, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 105, '2018-06-09 17:22:35', 'Y'),
(32, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 160, '2018-06-09 17:22:55', 'Y'),
(33, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 260, '2018-06-09 17:24:19', 'Y'),
(34, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 150, '2018-06-09 17:24:35', 'Y'),
(35, 2, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 240, '2019-03-12 15:13:54', 'Y'),
(36, 3, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 180, '2020-08-21 07:52:59', 'Y'),
(37, 3, 4, 'ทดสอบผู้ใช้ระบบ2', '0801813681', 180, '2020-08-21 07:55:41', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `od_id_ord` int(11) NOT NULL,
  `od_id_prd` int(11) NOT NULL,
  `od_num` int(11) NOT NULL,
  `od_approve` int(11) NOT NULL,
  `od_price` float NOT NULL,
  `od_status` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order_detail`
--

INSERT INTO `tb_order_detail` (`od_id_ord`, `od_id_prd`, `od_num`, `od_approve`, `od_price`, `od_status`) VALUES
(1, 4, 1, 1, 150, 'Y'),
(2, 5, 1, 1, 100, 'Y'),
(3, 6, 1, 0, 55, 'N'),
(4, 6, 1, 0, 55, 'N'),
(5, 6, 1, 0, 55, 'N'),
(6, 6, 1, 0, 55, 'N'),
(7, 6, 1, 0, 55, 'N'),
(8, 6, 1, 0, 55, 'N'),
(9, 6, 1, 0, 55, 'N'),
(10, 6, 1, 0, 55, 'N'),
(11, 6, 1, 0, 55, 'N'),
(12, 6, 1, 0, 55, 'N'),
(13, 6, 1, 0, 55, 'N'),
(14, 6, 1, 0, 55, 'N'),
(15, 6, 1, 0, 55, 'N'),
(16, 6, 1, 0, 55, 'N'),
(17, 6, 1, 0, 55, 'N'),
(18, 6, 1, 1, 55, 'Y'),
(19, 6, 1, 0, 55, 'N'),
(19, 4, 1, 0, 150, 'N'),
(21, 4, 1, 1, 150, 'Y'),
(21, 6, 1, 1, 55, 'Y'),
(22, 6, 1, 0, 55, 'N'),
(22, 4, 1, 0, 150, 'N'),
(23, 6, 1, 0, 55, 'N'),
(24, 6, 1, 0, 55, 'N'),
(25, 6, 1, 0, 55, 'N'),
(26, 6, 1, 0, 55, 'N'),
(27, 6, 1, 0, 55, 'N'),
(28, 6, 1, 0, 55, 'N'),
(29, 6, 1, 0, 55, 'N'),
(30, 6, 1, 0, 55, 'N'),
(31, 6, 1, 0, 55, 'N'),
(32, 6, 2, 0, 55, 'N'),
(33, 6, 2, 0, 55, 'N'),
(33, 5, 1, 0, 100, 'N'),
(34, 5, 1, 0, 100, 'N'),
(35, 7, 1, 0, 80, 'N'),
(35, 6, 2, 0, 55, 'N'),
(36, 7, 1, 0, 80, 'N'),
(37, 7, 1, 0, 80, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `temp_design`
--

CREATE TABLE `temp_design` (
  `tpid` bigint(20) NOT NULL,
  `type_id` int(11) NOT NULL,
  `color` varchar(255) NOT NULL,
  `frontside` varchar(255) NOT NULL,
  `backside` varchar(255) NOT NULL,
  `leftside` varchar(255) NOT NULL,
  `rightside` varchar(255) NOT NULL,
  `isDel` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `temp_design`
--

INSERT INTO `temp_design` (`tpid`, `type_id`, `color`, `frontside`, `backside`, `leftside`, `rightside`, `isDel`) VALUES
(1, 1, '#101011', 'images/temp/o/1.JPG', 'images/temp/o/2.JPG', 'images/temp/o/3.JPG', 'images/temp/o/4.JPG', 0),
(2, 1, '#FFFF33', 'images/temp/o/5.JPG', 'images/temp/o/6.JPG', 'images/temp/o/7.JPG', 'images/temp/o/8.JPG', 0),
(3, 1, '#f7d100', 'images/temp/o/9.JPG', 'images/temp/o/10.JPG', 'images/temp/o/11.JPG', 'images/temp/o/12.JPG', 0),
(4, 1, '#d33700', 'images/temp/o/13.JPG', 'images/temp/o/14.JPG', 'images/temp/o/15.JPG', 'images/temp/o/16.JPG', 0),
(5, 1, '#b35909', 'images/temp/o/17.JPG', 'images/temp/o/18.JPG', 'images/temp/o/19.JPG', 'images/temp/o/20.JPG', 0),
(6, 1, '#865106', 'images/temp/o/21.JPG', 'images/temp/o/22.JPG', 'images/temp/o/23.JPG', 'images/temp/o/24.JPG', 0),
(7, 1, '#9e0574', 'images/temp/o/25.JPG', 'images/temp/o/26.JPG', 'images/temp/o/27.JPG', 'images/temp/o/28.JPG', 0),
(8, 1, '#C70059', 'images/temp/o/29.JPG', 'images/temp/o/30.JPG', 'images/temp/o/31.JPG', 'images/temp/o/32.JPG', 0),
(9, 1, '#CC0000', 'images/temp/o/33.JPG', 'images/temp/o/34.JPG', 'images/temp/o/35.JPG', 'images/temp/o/36.JPG', 0),
(10, 1, '#cf1d43', 'images/temp/o/37.JPG', 'images/temp/o/38.JPG', 'images/temp/o/39.JPG', 'images/temp/o/40.JPG', 0),
(11, 1, '#FF69B4', 'images/temp/o/41.JPG', 'images/temp/o/42.JPG', 'images/temp/o/43.JPG', 'images/temp/o/44.JPG', 0),
(12, 1, '#ff74b5', 'images/temp/o/45.JPG', 'images/temp/o/46.JPG', 'images/temp/o/47.JPG', 'images/temp/o/48.JPG', 0),
(13, 1, '#CCFFFF', 'images/temp/o/49.JPG', 'images/temp/o/50.JPG', 'images/temp/o/51.JPG', 'images/temp/o/52.JPG', 0),
(14, 1, '#87CEFA', 'images/temp/o/53.JPG', 'images/temp/o/54.JPG', 'images/temp/o/55.JPG', 'images/temp/o/56.JPG', 0),
(15, 1, '#00BFFF', 'images/temp/o/57.JPG', 'images/temp/o/58.JPG', 'images/temp/o/59.JPG', 'images/temp/o/60.JPG', 0),
(16, 1, '#0074b6', 'images/temp/o/61.JPG', 'images/temp/o/62.JPG', 'images/temp/o/63.JPG', 'images/temp/o/64.JPG', 0),
(17, 1, '#00297b', 'images/temp/o/65.JPG', 'images/temp/o/66.JPG', 'images/temp/o/67.JPG', 'images/temp/o/68.JPG', 0),
(18, 1, '#001B2C', 'images/temp/o/69.JPG', 'images/temp/o/70.JPG', 'images/temp/o/71.JPG', 'images/temp/o/72.JPG', 0),
(19, 1, '#1d4d57', 'images/temp/o/73.JPG', 'images/temp/o/74.JPG', 'images/temp/o/75.JPG', 'images/temp/o/76.JPG', 0),
(20, 1, '#002f30', 'images/temp/o/77.JPG', 'images/temp/o/78.JPG', 'images/temp/o/79.JPG', 'images/temp/o/80.JPG', 0),
(21, 1, '#c8f435', 'images/temp/o/81.JPG', 'images/temp/o/82.JPG', 'images/temp/o/83.JPG', 'images/temp/o/84.JPG', 0),
(23, 1, '#9ACD32', 'images/temp/o/85.JPG', 'images/temp/o/86.JPG', 'images/temp/o/87.JPG', 'images/temp/o/87.JPG', 0),
(24, 1, '#008a31', 'images/temp/o/89.JPG', 'images/temp/o/90.JPG', 'images/temp/o/91.JPG', 'images/temp/o/92.JPG', 0),
(25, 1, '#EEAEEE', 'images/temp/o/93.JPG', 'images/temp/o/94.JPG', 'images/temp/o/95.JPG', 'images/temp/o/96.JPG', 0),
(26, 1, '#8968CD', 'images/temp/o/97.JPG', 'images/temp/o/98.JPG', 'images/temp/o/99.JPG', 'images/temp/o/100.JPG', 0),
(27, 1, '#36004c', 'images/temp/o/101.JPG', 'images/temp/o/102.JPG', 'images/temp/o/103.JPG', 'images/temp/o/104.JPG', 0),
(28, 1, '#D3D3D3', 'images/temp/o/105.JPG', 'images/temp/o/106.JPG', 'images/temp/o/107.JPG', 'images/temp/o/108.JPG', 0),
(29, 1, '#8B8989', 'images/temp/o/109.JPG', 'images/temp/o/110.JPG', 'images/temp/o/111.JPG', 'images/temp/o/112.JPG', 0),
(30, 1, '#696969', 'images/temp/o/113.JPG', 'images/temp/o/114.JPG', 'images/temp/o/115.JPG', 'images/temp/o/116.JPG', 0),
(31, 1, '#363636', 'images/temp/o/117.JPG', 'images/temp/o/118.JPG', 'images/temp/o/119.JPG', 'images/temp/o/120.JPG', 0),
(32, 1, '#ffffff', 'images/temp/o/123.JPG', 'images/temp/o/124.JPG', 'images/temp/o/122.JPG', 'images/temp/o/121.JPG', 0),
(33, 2, '#101011', 'images/temp/v/1.JPG', 'images/temp/v/2.JPG', 'images/temp/v/3.JPG', 'images/temp/v/4.JPG', 0),
(34, 2, '#FFFF33', 'images/temp/v/5.JPG', 'images/temp/v/6.JPG', 'images/temp/v/7.JPG', 'images/temp/v/8.JPG', 0),
(35, 2, '#f7d100', 'images/temp/v/9.JPG', 'images/temp/v/10.JPG', 'images/temp/v/11.JPG', 'images/temp/v/12.JPG', 0),
(36, 2, '#d33700', 'images/temp/v/13.JPG', 'images/temp/v/14.JPG', 'images/temp/v/15.JPG', 'images/temp/v/16.JPG', 0),
(37, 2, '#b35909', 'images/temp/v/17.JPG', 'images/temp/v/18.JPG', 'images/temp/v/19.JPG', 'images/temp/v/20.JPG', 0),
(38, 2, '#865106', 'images/temp/v/21.JPG', 'images/temp/v/22.JPG', 'images/temp/v/23.JPG', 'images/temp/v/24.JPG', 0),
(39, 2, '#9e0574', 'images/temp/v/25.JPG', 'images/temp/v/26.JPG', 'images/temp/v/27.JPG', 'images/temp/v/28.JPG', 0),
(40, 2, '#C70059', 'images/temp/v/29.JPG', 'images/temp/v/30.JPG', 'images/temp/v/31.JPG', 'images/temp/v/32.JPG', 0),
(41, 2, '#CC0000', 'images/temp/v/33.JPG', 'images/temp/v/34.JPG', 'images/temp/v/35.JPG', 'images/temp/v/36.JPG', 0),
(42, 2, '#cf1d43', 'images/temp/v/37.JPG', 'images/temp/v/38.JPG', 'images/temp/v/39.JPG', 'images/temp/v/40.JPG', 0),
(43, 2, '#FF69B4', 'images/temp/v/41.JPG', 'images/temp/v/42.JPG', 'images/temp/v/43.JPG', 'images/temp/v/44.JPG', 0),
(44, 2, '#ff74b5', 'images/temp/v/45.JPG', 'images/temp/v/46.JPG', 'images/temp/v/47.JPG', 'images/temp/v/48.JPG', 0),
(45, 2, '#CCFFFF', 'images/temp/v/49.JPG', 'images/temp/v/50.JPG', 'images/temp/v/51.JPG', 'images/temp/v/52.JPG', 0),
(46, 2, '#87CEFA', 'images/temp/v/53.JPG', 'images/temp/v/54.JPG', 'images/temp/v/55.JPG', 'images/temp/v/56.JPG', 0),
(47, 2, '#00BFFF', 'images/temp/v/57.JPG', 'images/temp/v/58.JPG', 'images/temp/v/59.JPG', 'images/temp/v/60.JPG', 0),
(48, 2, '#0074b6', 'images/temp/v/61.JPG', 'images/temp/v/62.JPG', 'images/temp/v/63.JPG', 'images/temp/v/64.JPG', 0),
(49, 2, '#00297b', 'images/temp/v/65.JPG', 'images/temp/v/66.JPG', 'images/temp/v/67.JPG', 'images/temp/v/68.JPG', 0),
(50, 2, '#001B2C', 'images/temp/v/69.JPG', 'images/temp/v/70.JPG', 'images/temp/v/71.JPG', 'images/temp/v/72.JPG', 0),
(51, 2, '#1d4d57', 'images/temp/v/73.JPG', 'images/temp/v/74.JPG', 'images/temp/v/75.JPG', 'images/temp/v/76.JPG', 0),
(52, 2, '#002f30', 'images/temp/v/77.JPG', 'images/temp/v/78.JPG', 'images/temp/v/79.JPG', 'images/temp/v/80.JPG', 0),
(53, 2, '#c8f435', 'images/temp/v/81.JPG', 'images/temp/v/82.JPG', 'images/temp/v/83.JPG', 'images/temp/v/84.JPG', 0),
(54, 2, '#9ACD32', 'images/temp/v/85.JPG', 'images/temp/v/86.JPG', 'images/temp/v/87.JPG', 'images/temp/v/87.JPG', 0),
(55, 2, '#008a31', 'images/temp/v/89.JPG', 'images/temp/v/90.JPG', 'images/temp/v/91.JPG', 'images/temp/v/92.JPG', 0),
(56, 2, '#EEAEEE', 'images/temp/v/93.JPG', 'images/temp/v/94.JPG', 'images/temp/v/95.JPG', 'images/temp/v/96.JPG', 0),
(57, 2, '#8968CD', 'images/temp/v/97.JPG', 'images/temp/v/98.JPG', 'images/temp/v/99.JPG', 'images/temp/v/100.JPG', 0),
(58, 2, '#36004c', 'images/temp/v/101.JPG', 'images/temp/v/102.JPG', 'images/temp/v/103.JPG', 'images/temp/v/104.JPG', 0),
(59, 2, '#D3D3D3', 'images/temp/v/105.JPG', 'images/temp/v/106.JPG', 'images/temp/v/107.JPG', 'images/temp/v/108.JPG', 0),
(60, 2, '#8B8989', 'images/temp/v/109.JPG', 'images/temp/v/110.JPG', 'images/temp/v/111.JPG', 'images/temp/v/112.JPG', 0),
(61, 2, '#696969', 'images/temp/v/113.JPG', 'images/temp/v/114.JPG', 'images/temp/v/115.JPG', 'images/temp/v/116.JPG', 0),
(62, 2, '#363636', 'images/temp/v/117.JPG', 'images/temp/v/118.JPG', 'images/temp/v/119.JPG', 'images/temp/v/120.JPG', 0),
(63, 2, '#ffffff', 'images/temp/v/123.JPG', 'images/temp/v/124.JPG', 'images/temp/v/122.JPG', 'images/temp/v/121.JPG', 0),
(64, 3, '#fffc00', 'images/temp/po/1.JPG', 'images/temp/po/2.JPG', 'images/temp/po/3.JPG', 'images/temp/po/4.JPG', 0),
(65, 3, '#f2470f', 'images/temp/po/5.JPG', 'images/temp/po/6.JPG', 'images/temp/po/7.JPG', 'images/temp/po/8.JPG', 0),
(66, 3, '#d20900', 'images/temp/po/9.JPG', 'images/temp/po/10.JPG', 'images/temp/po/11.JPG', 'images/temp/po/12.JPG', 0),
(67, 3, '#f54b98', 'images/temp/po/13.JPG', 'images/temp/po/14.JPG', 'images/temp/po/15.JPG', 'images/temp/po/16.JPG', 0),
(68, 3, '#be84dd', 'images/temp/po/17.JPG', 'images/temp/po/18.JPG', 'images/temp/po/19.JPG', 'images/temp/po/20.JPG', 0),
(69, 3, '#480e71', 'images/temp/po/21.JPG', 'images/temp/po/22.JPG', 'images/temp/po/23.JPG', 'images/temp/po/24.JPG', 0),
(70, 3, '#70d8eb', 'images/temp/po/25.JPG', 'images/temp/po/26.JPG', 'images/temp/po/27.JPG', 'images/temp/po/28.JPG', 0),
(71, 3, '#52a2e3', 'images/temp/po/29.JPG', 'images/temp/po/30.JPG', 'images/temp/po/31.JPG', 'images/temp/po/32.JPG', 0),
(72, 3, '#0154af', 'images/temp/po/33.JPG', 'images/temp/po/34.JPG', 'images/temp/po/35.JPG', 'images/temp/po/36.JPG', 0),
(73, 3, '#1f2684', 'images/temp/po/37.JPG', 'images/temp/po/38.JPG', 'images/temp/po/39.JPG', 'images/temp/po/40.JPG', 0),
(74, 3, '#030537', 'images/temp/po/41.JPG', 'images/temp/po/42.JPG', 'images/temp/po/43.JPG', 'images/temp/po/44.JPG', 0),
(75, 3, '#cfee00', 'images/temp/po/45.JPG', 'images/temp/po/46.JPG', 'images/temp/po/47.JPG', 'images/temp/po/48.JPG', 0),
(76, 3, '#00d300', 'images/temp/po/49.JPG', 'images/temp/po/50.JPG', 'images/temp/po/51.JPG', 'images/temp/po/52.JPG', 0),
(77, 3, '#999fae', 'images/temp/po/53.JPG', 'images/temp/po/54.JPG', 'images/temp/po/55.JPG', 'images/temp/po/56.JPG', 0),
(78, 3, '#3e3e3e', 'images/temp/po/57.JPG', 'images/temp/po/58.JPG', 'images/temp/po/59.JPG', 'images/temp/po/60.JPG', 0),
(79, 3, '#000000', 'images/temp/po/61.JPG', 'images/temp/po/62.JPG', 'images/temp/po/63.JPG', 'images/temp/po/64.JPG', 0),
(80, 3, '#ccff00', 'images/temp/po/65.JPG', 'images/temp/po/66.JPG', 'images/temp/po/67.JPG', 'images/temp/po/68.JPG', 0),
(81, 3, '#ff6239', 'images/temp/po/69.JPG', 'images/temp/po/70.JPG', 'images/temp/po/71.JPG', 'images/temp/po/72.JPG', 0),
(82, 3, '#ff3e8c', 'images/temp/po/73.JPG', 'images/temp/po/74.JPG', 'images/temp/po/75.JPG', 'images/temp/po/76.JPG', 0),
(83, 3, '#FFFFFF', 'images/temp/po/77.JPG', 'images/temp/po/78.JPG', 'images/temp/po/79.JPG', 'images/temp/po/80.JPG', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `board_answer`
--
ALTER TABLE `board_answer`
  ADD PRIMARY KEY (`ans_id`);

--
-- Indexes for table `board_question`
--
ALTER TABLE `board_question`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `design`
--
ALTER TABLE `design`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mb_id` (`mb_id`);

--
-- Indexes for table `design_order`
--
ALTER TABLE `design_order`
  ADD PRIMARY KEY (`ord_id`);

--
-- Indexes for table `design_order_detail`
--
ALTER TABLE `design_order_detail`
  ADD PRIMARY KEY (`od_id_order`);

--
-- Indexes for table `ems`
--
ALTER TABLE `ems`
  ADD PRIMARY KEY (`ems_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`mb_id`) USING BTREE;

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prd_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`ord_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bn_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `board_answer`
--
ALTER TABLE `board_answer`
  MODIFY `ans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `board_question`
--
ALTER TABLE `board_question`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `design`
--
ALTER TABLE `design`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `design_order`
--
ALTER TABLE `design_order`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `design_order_detail`
--
ALTER TABLE `design_order_detail`
  MODIFY `od_id_order` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ems`
--
ALTER TABLE `ems`
  MODIFY `ems_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `mb_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `prd_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `ord_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
