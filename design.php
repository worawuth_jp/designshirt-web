<?php
session_start();
include "function.php";
include "function-page.php";
include "connect_db.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <title>ร้านค้าออนไลน์</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
    <script type="text/javascript" src="js/html2canvas.min.js"></script>
    <link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css_style_index.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_menu.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_board.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_page.css"/>
    <link rel="stylesheet" type="text/css" href="css_style_design.css"/>

    <!--include font -->
    <?php include './font-include.php'; ?>

</head>
<body id="Page_design">
<div id="container">
    <div id="bander_front">
        <?php include "bander_front.php"; ?>
        <div id="menu_top_design">
            <?PHP include "menu_top1.php"; ?>
        </div>
    </div>

    <style>
        #click:hover {
            background-color: #D0D0D0;
        }
    </style>


    <div class="menu_left"><!-- เมนูด้านซ้าย -->
        <?PHP include "menu_left_front.php"; ?>
    </div><!-- จบเมนูด้านซ้าย -->

    <div class="row">
        <div id="select-type" class="col-md-2">
            <select id="stype" class="col-md-12 p-0 pb-2 pt-2 mt-2 mb-2">
                <option value="1">คอกลม</option>
                <option value="2">คอวี</option>
                <option value="3">คอปก</option>
            </select>
            <!--preview choose-->
            <div class="border cursor-hand col-md-12 pl-0 pr-0">
                <a href="#" onclick="change_preview('front')"><img id="F" class="img-fluid"
                                                                   src="./images/icon_board/Front.jpg"></a>
            </div>

            <div class="border cursor-hand col-md-12 pl-0 pr-0">
                <a href="#" onclick="change_preview('back')"><img id="B" class="img-fluid"
                                                                  src="./images/icon_board/BACK.jpg"></a>
            </div>

            <div class="border cursor-hand col-md-12 pl-0 pr-0">
                <a href="#" onclick="change_preview('left')"><img id="L" class="img-fluid"
                                                                  src="./images/icon_board/LEFT.jpg"></a>
            </div>

            <div class="border cursor-hand col-md-12 pl-0 pr-0">
                <a href="#" onclick="change_preview('right')"><img id="R" class="img-fluid"
                                                                   src="./images/icon_board/RIGHT.jpg"></a>
            </div>

            <!--Color choose-->
            <div class=" col-md-12 pb-2 pr-0 border" id="cselect">
                <?php
                $sql = "SELECT * FROM `temp_design` WHERE isDel = 0 and type_id=1";
                if ($result = mysqli_query($con, $sql)) {
                    $count = 0;
                    $a = 0;
                    $i = 0;
                    $query = mysqli_query($con, $sql);
                    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {

                        if ($count == 0) {
                            echo "<div class=\"row pl-1 pt-1\">";
                            $a = $a + 1;
                        }
                        echo "<a href=\"#\"
                               onclick=\"change_color('" . $row['frontside'] . "','" . $row['backside'] . "','" . $row['leftside'] . "','" . $row['rightside'] . "')\">
                                <div class=\"border mt-1 ml-1\"
                                     style=\"height: 20px;width: 20px;background-color: " . $row['color'] . "\"></div></a>";
                        $count = $count + 1;
                        if ($count == 3) {
                            echo "</div>";
                            $count = 0;
                            $a = $a + 1;
                        }
                    }
                    if($a%2 != 0){
                        echo "</div>";
                    }
                }
                ?>

            </div>
        </div>
        <!--<i class="fa fa-square-full" style="font-size: 20px"></i>-->
        <!--Preview Zone-->
        <i class="fa fa-square-full" style="font-size: 20px"></i>
        <!--Preview Zone-->
        <style>

            .cursor-all-scroll {
                cursor: all-scroll;
            }

            .text-drag {
                cursor: all-scroll;
                position: absolute;
                z-index: 10;
                font-size: 50px;
                padding: 5px 5px 2px 2px;
            }

            .border-fill {
                border-style: dashed;
                border-color: red;
            }

            .cursor-resize {
                cursor: nwse-resize;
            }

        </style>

        <div id="preview_img" class="col-md-10 p-0 "
             style="background-image: url('images/temp/o/1.jpg');background-repeat: no-repeat;background-size: auto;background-position: center;">
            <div id="areabox" class="ml-auto mr-auto border-text hidden-filed "
                 style="height: 85%;width:55%;margin-top: 8%;"></div>
        </div>
    </div>
    <div id="footer_front">
        <div class="data_footer">
            <p>
                <?php include "footer.php"; ?>
                <?PHP
                if ($_POST['add_basket']) {
                    echo "<meta http-equiv='refresh' content='0; url=add_basket_product.php?ID=" . $_POST['ID'] . "&txt_num=1'>";
                } else if ($_POST['btn_detail']) {
                    echo "<meta http-equiv='refresh' content='0; url=detail_product.php?ID=" . $_POST['ID'] . "'>";
                }
                ?>
            </p>

        </div>

    </div>
    <div style="clear:both;"></div>
    <!-- End menu -->
</div>
<!-- end Container -->

<i id="btn-tool" class="fa fa-angle-double-right border cursor-hand btn-tool-open"
   style="font-size: 30px;float: right"></i>
<div id="tool" class="bg-white border open-tool">
    <div class="col-md-12 p-0 pr-1 text-right font-weight-bold" style="font-size: 14px;cursor: pointer;color: blue">
        <a id="reset">reset</a>
    </div>
    <div id="click" class="border-s2 col-md-10 ml-auto mr-auto mt-2 mb-2 text-center cursor-hand pt-2 text-tool">
        <label class="cursor-hand" style="font-size: 20px;color: grey">
            <i class="fa fa-font" style="font-size: 30px;color: #353535"></i><br>
            ADD TEXT
        </label>
    </div>

    <div id="TEXT" class="col-md-10 ml-auto mr-auto pl-0 pr-0 mb-2 hidden-filed">
        <label class="text-left font-weight-bold">ใส่ข้อความของคุณ</label><br>
        <input id="txt" class="col-md-12 pl-2 pr-2">
        <button id="btn-text-on" class="btn btn-info pt-1 mt-2 pb-1">ตกลง</button>
    </div>

    <div id="click" class="border-s2 col-md-10 ml-auto mr-auto mb-2 text-center cursor-hand pt-2 pic-tool">
        <label class="cursor-hand" style="font-size: 20px;color: grey">
            <i class="fa fa-image" style="font-size: 30px;color: #353535"></i><br>
            ADD IMAGE
        </label>
    </div>

    <div id="PIC" class="col-md-10 ml-auto mr-auto pl-0 pr-0 mb-2 hidden-filed">
        <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
            <label class="text-left font-weight-bold">อัพโหลดรูปภาพ</label>
            <input type="file" id="img" name="img" accept="image/*" onchange="loadFile(event)" hidden/>
            <button id="up_logo" class="btn btn-info pt-1 pb-1" type="button">อัพโหลด</button>
        </form>
    </div>

    <!--property text-->
    <div id="TEXT-2" class="col-md-10 ml-auto mr-auto mt-3 pl-0 pr-0 mb-2 hidden-filed">
        <input id="tellID" name="tellID" type="hidden">
        <label class="text-left font-weight-bold">ปรับแต่งข้อความ</label><br>
        <input id="txt-prop" class="col-md-12 pl-2 pr-2">
        <label class="text-left font-weight-bold mt-2">font:</label>
        <select class="col-md-10 pl-0 mb-2 sarabun" id="font-prop" style="font-size: 16px">
            <option class="sarabun" value="sarabun">Sarabun</option>
            <option class="roboto" value="roboto">Roboto</option>
            <option class="kanit" value="kanit">Kanit</option>
            <option class="prompt" value="prompt">Prompt</option>
            <option class="raleway" value="raleway">Raleway</option>
            <option class="sansita" value="sansita">Sansita</option>
            <option class="amatic-sc" value="amatic-sc">Amatic SC</option>
            <option class="berkshire-swash" value="berkshire-swash">Berkshire Swash</option>
            <option class="dancing-script" value="dancing-script">Dancing Script</option>
            <option class="itim" value="itim">Itim</option>
            <option class="kaushan-script" value="kaushan-script">Kaushan Script</option>
            <option class="mali" value="mali">Mali</option>
            <option class="pacifico" value="pacifico">Pacifico</option>
        </select><br>
        <label class="text-left font-weight-bold">size:</label>
        <input type="number" id="fsize" min="0" max="999" value="10"><br>
        <label class="text-left font-weight-bold">color:</label>
        <div id="color-picker-1" class="mx-auto"></div>
        <input type="color" id="color_picker" name="color" class="p-0" value="#000000"><br>
    </div>

    <div class="col-md-12" style="font-size: 16px">
        <input type="hidden" id="picsize"/>
        <label class="font-weight-bold">ราคา(บาท) : </label><br>
        <label id="showprice" class="font-weight-normal roboto" style="color: blue"
               style="font-size: 18px;">150.00</label><br>
    </div>

    <div class="mb-auto p-1">
        <button id="btn-save" class="btn btn-success pt-1 pb-1 col-md-12">บันทึกแบบเสื้อ</button>
        <a href="history_design.php?mb_id=<?php echo $_SESSION['sess_mb_id'] ?>">
            <button class="btn btn-secondary mt-2 pt-1 pb-1 col-md-12">ประวัติการออกแบบ</button>
        </a>
    </div>
</div>

<!--Script JS-->

<?php
function console($msg)
{
    echo '<script>';
    echo 'console.log(' . $msg . ')';
    echo '</script>';
}

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dom-to-image/2.6.0/dom-to-image.min.js"></script>
<script src="js/fileSaver.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="js/design.js"></script>
<script type="text/javascript" src="js/html2canvas.min.js"></script>
</body>
</html>
