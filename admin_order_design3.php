<?php
include "session_admin.php";
include "function.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>ร้านค้าออนไลน์</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
    <script type="text/javascript" src="images/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="slimbox/js/slimbox2.js"></script>
    <link rel="stylesheet" href="slimbox/css/slimbox2.css" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" href="css_style_index.css" />
    <link rel="stylesheet" type="text/css" href="css_style_menu.css" />
    <link rel="stylesheet" type="text/css" href="css_style_board.css" />
    <link rel="stylesheet" type="text/css" href="css_style_page.css" />

    <style type="text/css">
    </style>
</head>
<body id="Page1">
<div id="container">
    <div id="bander_back">
        <?PHP include "bander_back.php"; ?>
        <div id="menu_top">
            <p>
                <?PHP include "menu_top2.php"; ?>
            </p>
        </div>
    </div>

    <div class="menu_left"><!-- เมนูด้านซ้าย -->
        <?PHP  include "menu_left_back.php"; ?>
    </div><!-- จบเมนูด้านซ้าย -->

    <div class="data_center"><!-- ส่วนกลางของเว็บ -->
        <div class="data_center_back">
            <table width="100%" height="850" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top"><div class="title">
                            <h2><img src="images/diagram-60.png" width="48" height="48" /> ข้อมูลใบสั่งทำเสื้อ รอการจัดส่ง</h2>
                        </div>
                        <br><br>
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <thead>
                                <th>#</th>
                                <th>รูปที่ออกแบบ</th>
                                <th>ราคา</th>
                                <th>สถานะ</th>
                                <th></th>
                                </thead>
                            </tr>
                            <?php
                            $sql = "SELECT * FROM design_order INNER JOIN design ON design.id = design_order.ord_design_id WHERE ord_status='N'";
                            $result = $con->query($sql);
                            $count = 0;
                            while ($row = $result->fetch_assoc()) {
                                $count = $count + 1;
                                ?>
                                <tr>
                                    <td style="text-align: center"><?php echo $count; ?></td>
                                    <td style="text-align: center"><img height="180px" src="<?php echo $row['pic'] ?>">
                                    </td>

                                    <td style="text-align: center"><?php echo $row['price'] ?></td>
                                    <td style="text-align: center">
                                        <div style="font-size: 14px">
                                            <?php
                                            if($row['ord_status'] == 'Y'){
                                                echo "<samp style='color: red;'>ยังไม่ชำระเงิน</samp>";
                                            }else if($row['ord_status']=='1'){ // 1 คือสถานะ คือ แจ้งชำระเงินผ่านหน้าเว็บไซต์
                                                echo "<samp style='color: green;'>กำลังตรวจสอบ</samp>";

                                            }else if($row['ord_status']=='2'){ // 2 คือสถานะ คือ ตรวจสอบการชำระเงินแล้ว
                                                echo "<samp style='color: blue;'>ชำระเงินแล้ว</samp>";

                                            }else if($row['ord_status']=='3'){ // 3 คือสถานะ คือ อนุมัติสินค้าแล้ว
                                                echo "<samp style='color: #FF6600;'>อนุมัติสินค้าแล้ว</samp>";

                                            }else if($row['ord_status']=='4'){ // 4 คือสถานะ รอส่งสินค้า
                                                echo "<samp style='color: #660033;'>รอส่งสินค้า</samp>";

                                            }else if($row['ord_status']=='5'){ // 5 คือสถานะ ส่งสินค้าแล้ว
                                                echo "<samp style='color: #6633FF;'>ส่งสินค้าแล้ว</samp>";

                                            }else if($row['ord_status']=='N'){ // N คือสถานะ ยกเลิกใบสั่งซื้อสินค้า
                                                echo "<samp style='color: #666;'>ยกเลิกแล้ว</samp>";

                                            }
                                            else{
                                                echo "<samp style='color: red;'>ไม่มีข้อมูล</samp>";
                                            }
                                            ?>
                                        </div>
                                    </td>
                                    <td style="text-align: center">
                                        <a href="design_cancel.php?id=<?php echo $row['ord_id'] ?>">
                                            <button>ยกเลิก</button>
                                        </a></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>

                </tr>
            </table>
            <p>&nbsp;</p>
        </div>

        <!-- เมนูด้านซ้าย -->
        <p style="clear:both;"></p>
        <!-- ปิด เมนูด้านซ้าย -->

    </div>
    <div id="footer_front">
        <div class="data_footer">
            <p>
                <?PHP include "footer.php"; ?>
            </p>

        </div>

    </div>
    <div style="clear:both;"></div>
    <!-- End menu -->
</div>
<!-- end Container -->
</body>
</html>
